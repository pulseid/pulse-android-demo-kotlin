package com.pulse.demo

import android.Manifest
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.TextView

import com.pulseid.sdk.PulseEvents
import com.pulseid.sdk.PulseSdk

import android.Manifest.permission.ACCESS_FINE_LOCATION
import kotlinx.android.synthetic.main.activity_demo.*

class MainActivity : AppCompatActivity() {

    private lateinit var pulseSdk: PulseSdk

    private val startupReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val intentAction = intent.action

            if (PulseEvents.SDK_STARTED == intentAction) {
                Log.i(TAG, "SDK Started!")
            } else {
                Log.i(TAG, "SDK Stopped!")
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(com.pulse.demo.R.layout.activity_demo)

        sdkVersionName.text = BuildConfig.VERSION_NAME

        pulseSdk = PulseSdkApplication.instance.pulseSdk

        val startupIntentFilter = IntentFilter()
        startupIntentFilter.addAction(PulseEvents.SDK_STARTED)
        startupIntentFilter.addAction(PulseEvents.SDK_STOPPED)
        LocalBroadcastManager.getInstance(this).registerReceiver(startupReceiver,
                startupIntentFilter)

        //If Build.OS above Android L then permission granted will be false and requestpermissions() will execute
        // else sdk will start as permissions are already granted for Android L or below

        if (ActivityCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions()
        } else {
            pulseSdk.start(this@MainActivity)
        }
    }

    private fun requestPermissions() {
        ActivityCompat.requestPermissions(this@MainActivity,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                REQUEST_PERMISSIONS_REQUEST_CODE)
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>,
                                            grantResults: IntArray) {
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.size <= 0) {
                Log.i(TAG, "User interaction was cancelled.")
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.i(TAG, "Permission granted.")
                //Step #2: Start the SDK
                pulseSdk.start()
            } else {
                Log.i(TAG, "Permission denied")
            }
        }
    }

    companion object {
        private val TAG = "MainActivity"
        val REQUEST_PERMISSIONS_REQUEST_CODE = 42
    }
}