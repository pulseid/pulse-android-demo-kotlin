package com.pulse.demo

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_deeplink.*

class DeepLinkActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_deeplink)
        val intent = intent
        val data = intent.data
        if (data != null) {
            tv_deeplink.text = data.host
        }
    }
}
