package com.pulse.demo

import android.app.Application

import com.pulseid.sdk.PulseSdk
import com.pulseid.sdk.PulseSdkConfig

class PulseSdkApplication : Application() {

    companion object {
        val TAG = "PulseSdkApplication"

        private val APP_KEY = "YOUR_APP_KEY" // TODO: Replace with your own!
        private val APP_URL = "YOUR_APP_URL" // TODO: Replace with your own!

        lateinit var instance: PulseSdkApplication
            private set
    }

    lateinit var pulseSdk: PulseSdk
        private set

    override fun onCreate() {
        super.onCreate()

        instance = this

        val config = PulseSdkConfig(APP_KEY, APP_URL)
        pulseSdk = PulseSdk(this, config)
    }
}